-module(evaluator_app).

-behaviour(application).

-define(APPS, [evaluator]).

-import(msgpack, [pack/2, unpack/2, pack/1, unpack/1]).

-export([start/3,start/2,stop/1,evaluation/0,eval/2,fillSpawned/3,llenarDB/4,insertarEspacio/3,writeCustomer/1,extractExample/1,install/0,rule_hashId_0cdc25b536f0c0f52e54f90402c11e17/3,rule_1/6,rule_hashId_0cdc25b536f0c0f52e54f90402c11e18/3,rule_2/6,rule_hashId_0cdc25b536f0c0f52e54f90402c11e19/3,rule_3/6,rule_hashId_0cdc25b536f0c0f52e54f90402c11e20/3,rule_4/6,rule_5/6,rule_6/6,rule_7/6,rule_8/6,rule_9/6,rule_10/6,rule_11/6,rule_12/6,rule_13/6,rule_14/6,rule_15/6,rule_16/6,rule_17/6,rule_18/6,rule_19/6,rule_20/6,rule_21/6,rule_22/6,rule_23/6,rule_24/6,rule_25/6,rule_26/6,rule_27/6,rule_28/6,rule_29/6,rule_30/6,rule_31/6,rule_32/6,rule_33/6,rule_34/6,rule_35/6,rule_36/6,rule_37/6,rule_38/6,rule_39/6,rule_40/6,rule_41/6,rule_42/6,rule_43/6,rule_44/6,rule_45/6,rule_46/6,rule_47/6,rule_48/6,rule_49/6,rule_50/6]).

-record(customer, {id, trx, src, sts, last}).

-define(Transaction, [{<<"0cdc25b536f0c0f52e54f90402c11e17">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"0cdc25b536f0c0f52e54f90402c11e18">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"0cdc25b536f0c0f52e54f90402c11e19">>,{[{<<"groupBy">>,{[{<<"BK">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"Macdonalds">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"Subway">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"KFC">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]}]}}]}},{<<"0cdc25b536f0c0f52e54f90402c11e20">>,{[{<<"groupBy">>,{[{<<"BK_6010">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"Macdonalds_6010">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"Subway_6010">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"KFC_6010">>,[{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,1433440200000},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]}]}}]}}]).

rulesList()->[rule_1,rule_2,rule_3,rule_4,rule_5,rule_6,rule_7,rule_8,rule_9,rule_10,rule_11,rule_12,rule_13,rule_14,rule_15,rule_16,rule_17,rule_18,rule_19,rule_20,rule_21,rule_22,rule_23,rule_24,rule_25,rule_26,rule_27,rule_28,rule_29,rule_30,rule_31,rule_32,rule_33,rule_34,rule_35,rule_36,rule_37,rule_38,rule_39,rule_40,rule_41,rule_42,rule_43,rule_44,rule_45,rule_46,rule_47,rule_48,rule_49,rule_50].

rulesHashIdList()->[rule_hashId_0cdc25b536f0c0f52e54f90402c11e17,rule_hashId_0cdc25b536f0c0f52e54f90402c11e18,rule_hashId_0cdc25b536f0c0f52e54f90402c11e19,rule_hashId_0cdc25b536f0c0f52e54f90402c11e20].

fillSpawned(Start, TotalBloque, Total) ->
    Hilos = Total/TotalBloque,
    llenarDB(Hilos, Start, TotalBloque+Start, TotalBloque).

llenarDB(0.0, _, _, _) -> fin;
llenarDB(Hilos, Start, End, TotalBloque) ->
    spawn(evaluator_app, insertarEspacio, [false, Start, End]),
    llenarDB(Hilos-1, Start+TotalBloque, End+TotalBloque, TotalBloque).

insertarEspacio(true, _, _) -> fin;
insertarEspacio(false, Start, End) -> 
    Id = list_to_binary(integer_to_list(Start)),
    writeCustomer(Id),
    NStart = Start+1,
    insertarEspacio((NStart > End), NStart, End).

writeCustomer(Id) -> 
    Write = fun() -> mnesia:write(#customer{id=Id,trx=?Transaction,src=[],sts=[]})  end,
    ok = mnesia:activity(transaction, Write, [], mnesia_frag).

extractExample(UserId) ->
    ReadCustomer = fun(IdCust) -> mnesia:read({customer, IdCust}) end,
    Data = mnesia:activity(transaction, ReadCustomer, [UserId], mnesia_frag),
    Data.

evaluation() -> receive {Pid, Transaction} -> eval(Pid, Transaction) end.

%Inicia evaluación
%Transacción a evaluar
eval(Pid, Transaction) -> HasCategory2 = true, HasLast = false, evalTransaction(Pid, HasCategory2, HasLast, Transaction).

%Evaluación transaccional
%Argumento 1 = Indica si contiene operaciones categoria 2
%Argumento 2 = Indica si contiene operaciones de tipo LAST
%Argumento 3 = Transacción a evaluar
evalTransaction(Pid, true, _, CurrentTrx) ->
    {_, _, MicroI} = os:timestamp(),
    EvTimestamp = get_timestamp(),
    UserId = proplists:get_value(<<"UserId">>, CurrentTrx),
    UserSpace = extractUserSpace(UserId),
    TrxsGroups = getTrxsGroups(UserSpace),
    LastTrx = getLast(UserSpace),
    {HashIdList,HashIdsGroups} = checkHashIds(TrxsGroups, CurrentTrx, EvTimestamp),
    %
    Self = self(),
    launch(Pid, Self, rulesList(), 0, TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp),
    {_, _, MicroF} = os:timestamp(),
    io:format("Duracion = ~p ~n", [MicroF-MicroI]),
    spaceData(CurrentTrx, HashIdList, HashIdsGroups),
    ok;
    %lists:foreach(fun(RuleFunc) -> spawn(?MODULE, RuleFunc, [TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp]) end, rulesList());
evalTransaction(Pid, false, true, CurrentTrx) ->
    UserId = proplists:get_value(<<"UserId">>, CurrentTrx),
    UserSpace = extractUserSpace(UserId),
    %{LastTrx} = proplists:get_value(<<"last">>, UserSpace),
    LastTrx = getLast(UserSpace),
    %
    Self = self(),
    launch(Pid, Self, rulesList(), 0, undefined, CurrentTrx, undefined, LastTrx, undefined),
    ok;
    %lists:foreach(fun(RuleFunc) -> spawn(?MODULE, RuleFunc, [undefined, CurrentTrx, undefined, LastTrx, undefined]) end, rulesList());
evalTransaction(Pid, false, false, CurrentTrx) ->
    Self = self(),
    launch(Pid, Self, rulesList(), 0, undefined, CurrentTrx, undefined, undefined, undefined),
    ok.
    %lists:foreach(fun(RuleFunc) -> spawn(?MODULE, RuleFunc, [undefined, CurrentTrx, undefined, undefined, undefined]) end, rulesList()).

spaceData(Transaction, HashIdList, HashIdsGroups) ->
    RWTrx = [{<<"trx">>,Transaction}|[]],
    RWHid = [{<<"hashIds">>,HashIdList}|RWTrx],
    [{<<"hashIdsGroups">>,HashIdsGroups}|RWHid].

getTrxsGroups([]) -> undefined;
getTrxsGroups(UserSpace) -> 
    [Data] = UserSpace, 
    TrxsGroups = Data#customer.trx,
    TrxsGroups.

getLast([]) -> undefined;
getLast(UserSpace) -> 
    [Data] = UserSpace, 
    LastTrx = Data#customer.last,
    LastTrx.

launch(Pid, _, [], N, _, _, _, _, _) -> invokeCollect(Pid, N);
launch(Pid, Self, Rules, N, TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp) ->
    RuleFunc = hd(Rules),
    spawn(?MODULE,RuleFunc,[TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Self]),
    launch(Pid, Self, tl(Rules), N+1, TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp).

invokeCollect(Pid,N) -> collect(Pid,N,[]).

%######################################################################
%###########FUNCIONES DE RESPUESTA A LAS FUNCIONES DE REGLAS###########
%######################################################################
%Respuesta = el primer elemento que acierta
%%collect(Pid,0,L) -> Pid ! L, L; collect(Pid,N,L) -> receive {W,true,R} -> {Pos,List} = resultsData(N,R,L), collect(Pid,Pos,List); {W,false,R} -> collect(Pid, N-1,L) end.
%ResultData - Respuesta = el primer elemento que acierta
%resultsData(N, 0, L) -> {N-1, L}; resultsData(N, R, L) -> {N-1, [R|L]}.
%%%--------------------------------------------------------------------
%%%--------------------------------------------------------------------
%Respuesta = todos los aciertos
%%collect(Pid,0,L) -> NL = lists:usort(L), Pid ! NL, NL; collect(Pid,N,L) -> receive {W,true,R} -> {Pos,List} = resultsData(N,R,L), collect(Pid,Pos,List); {W,false,R} -> collect(Pid,N-1,L) end.
%ResultData - Respuesta = todos los aciertos
%resultsData(N, 0, L) -> {N-1, L}; resultsData(N, R, L) -> {N-1, [R|L]}.
%######################################################################
%######################################################################
%######################################################################

collect(Pid,0,L) -> NL = lists:usort(L), Pid ! NL, NL; collect(Pid,N,L) -> receive {W,true,R} -> {Pos,List} = resultsData(N,R,L), collect(Pid,Pos,List); {W,false,R} -> collect(Pid,N-1,L) end.

resultsData(N, 0, L) -> {N-1, L}; resultsData(N, R, L) -> {N-1, [R|L]}.

extractUserSpace(UserId) ->
    %{UserTransactions} = {[{<<"id">>,<<"1">>},{<<"trxs">>,{[{<<"0cdc25b536f0c0f52e54f90402c11e19">>,{[{<<"groupBy">>,{[{<<"BK">>,[{[{<<"t">>,12345930940},{<<"amount">>,2500}]},{[{<<"t">>,12345930940},{<<"amount">>,3000}]}]},{<<"Macdonalds">>,[{[{<<"t">>,12345930940},{<<"amount">>,2500}]},{[{<<"t">>,12345930940},{<<"amount">>,3000}]},{[{<<"t">>,12345930940},{<<"amount">>,2500}]},{[{<<"t">>,12345930940},{<<"amount">>,3000}]},{[{<<"t">>,12345930940},{<<"amount">>,2500}]},{[{<<"t">>,12345930940},{<<"amount">>,3000}]}]}]}}]}},{<<"0cdc25b536f0c0f52e54f90402c11e20">>,{[{<<"groupBy">>,{[{<<"BK_6010">>,[{[{<<"t">>,12345930940},{<<"amount">>,2500}]},{[{<<"t">>,12345930940},{<<"amount">>,3000}]}]},{<<"Macdonalds_6010">>,[{[{<<"t">>,12345930940},{<<"amount">>,2500}]},{[{<<"t">>,12345930940},{<<"amount">>,3000}]},{[{<<"t">>,12345930940},{<<"amount">>,2500}]},{[{<<"t">>,12345930940},{<<"amount">>,3000}]},{[{<<"t">>,12345930940},{<<"amount">>,2500}]},{[{<<"t">>,12345930940},{<<"amount">>,3000}]}]}]}}]}}]}}]},
    %{UserTransactions} = {[{<<"id">>,<<"1">>},{<<"trxs">>,{[{<<"0cdc25b536f0c0f52e54f90402c11e17">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"0cdc25b536f0c0f52e54f90402c11e18">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"0cdc25b536f0c0f52e54f90402c11e19">>,{[{<<"groupBy">>,{[{<<"BK">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"Macdonalds">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"Subway">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"KFC">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]}]}}]}},{<<"0cdc25b536f0c0f52e54f90402c11e20">>,{[{<<"groupBy">>,{[{<<"BK_6010">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"Macdonalds_6010">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"Subway_6010">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"HND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"ECU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]},{<<"KFC_6010">>,[{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"BR">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"CND">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"MX">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]},{[{<<"t">>,12345930940},{<<"Monto">>,3000},{<<"Pais">>,<<"EEUU">>}]}]}]}}]}}]}},{<<"Last">>,{[{<<"t">>,12345930940},{<<"Monto">>,2500},{<<"Pais">>,<<"CR">>},{<<"MCC">>,<<"1313">>}]}}]},
    %UserTransactions.
    ReadCustomer = fun(IdCust) -> mnesia:read({customer, IdCust}) end,
    Read = mnesia:activity(transaction, ReadCustomer, [UserId], mnesia_frag),
    Read.

%Construye el listado de HashIds con los que coincide la transacción
checkHashIds(TrxsGroups, CurrentTrx, EvTimestamp) ->
    HashIdListFunc = rulesHashIdList(),
    checkHashIdFunc(HashIdListFunc, [], [], TrxsGroups, CurrentTrx, EvTimestamp).

%Evalua recursivamente las funciones de HashList
%Argumento 1 = Listado de funciones a validar
%Argumento 2 = Listado de HashIds que cumplen
%Argumento 3 = Grupos de transacciones
%Argumento 4 = Transacción actual
checkHashIdFunc([], HashIdList, HashIdsGroups, _, _, EvTimestamp) ->
    {HashIdList,HashIdsGroups};
checkHashIdFunc(HashIdListFunc, HashIdList, HashIdsGroups, TrxsGroups, CurrentTrx, EvTimestamp) ->
    HashIdFunc = hd(HashIdListFunc),
    {Status,HashId,Field} = erlang:apply(?MODULE, HashIdFunc, [TrxsGroups, CurrentTrx, EvTimestamp]),
    {NHashIdList, NHashIdGroups} = defineHashIdList(Status, HashId, Field, HashIdList, HashIdsGroups),
    checkHashIdFunc(tl(HashIdListFunc), NHashIdList, NHashIdGroups, TrxsGroups, CurrentTrx, EvTimestamp).

%Construye el resultado de la evaluación de una función de HashList
%La respuesta es una tupla de 2 elementos
%1 -> La lista de HashIds que se cumplieron
%2 -> La lista de HashIds|Campos que se cumplieron para elementos GroupBy
defineHashIdList(true, HashId, "", HashIdList, HashIdsGroups) ->
    NHashIdList = [HashId|HashIdList],
    {NHashIdList, HashIdsGroups};
defineHashIdList(true, HashId, Field, HashIdList, HashIdsGroups) ->
    NHashIdList = [HashId|HashIdList],
    NHashIdGroups = [{HashId,Field}|HashIdsGroups],
    {NHashIdList, NHashIdGroups};
defineHashIdList(false, _, _, HashIdList, HashIdsGroups) ->
    {HashIdList, HashIdsGroups}.

%Extrae un valor del Json a partir del key
valueJson(Key, JsObj) -> proplists:get_value(Key, JsObj).

%Evalúa un criterio categoría 2
%Los argumentos son especificados en la invocación - Construida en Node
%Arg 1 - Tipo de Operación a ejecutar SUM, COUNT, AVG
%Arg 2 - Considerar o no la transacción actual
%Arg 3 - Campo que se utiliza en la sumatoria o el average
%Arg 4 - Operador de consulta >, <, >=, etc
%Arg 5 - Valores comparativos
%Arg 6 - Límite de tiempo expresado en milisegundos
%Arg 7 - HashId asociado al criterio
%Arg 8 - Json de Grupos de Transacciones
%Arg 9 - Transacción actual
%Arg 10 - Listado de HashId que cumplieron
evalCategory2(_, _, _, _, _, _, _, undefined, _, _, _) -> false;
evalCategory2(CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp) ->
    routeExecEval(checkTakeActual(TakeActual, HashId, HashIdList), 
        CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp).

%Evaluacion dependiendo del argumento TakeActual
%Arg 1 - TakeActual - True evalua si la transaccion actual cumple - False no se evalua y prosigue
%Arg 2 - HashId que se evalua
%Arg 3 - HashIds que se cumplieron en la evaluacion inicial
checkTakeActual(true, HashId, HashIdList) ->
    lists:member(HashId, HashIdList);
checkTakeActual(false, _, _) ->
    true.

%Rutea a la evaluación de reglas categoría 2 o retorna false inmediatamente
%El primer armento se determina al evaluar o no la transacción actual (key TakeActual)
routeExecEval(false, _, _, _, _, _, _, _, _, _, _, _) ->
    false;
routeExecEval(true, CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp) ->
    Transactions = hashIdTransactions(HashId, TrxsGroups),
    TimeLimit = EvTimestamp - TimeMillis,
    routeOperation(CatOperation, TakeActual, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx).

%Evalúa un criterio categoría 2 para un elemento HASHID
%Los argumentos son especificados en la invocación - Construida en Node
%Arg 1 - Tipo de Operación a ejecutar SUM, COUNT, AVG
%Arg 2 - Considerar o no la transacción actual
%Arg 3 - Campo que se utiliza en la sumatoria o el average
%Arg 4 - Operador de consulta >, <, >=, etc
%Arg 5 - Valores comparativos
%Arg 6 - Límite de tiempo expresado en milisegundos
%Arg 7 - HashId asociado al criterio
%Arg 8 - Json de Grupos de Transacciones
%Arg 9 - Transacción actual
evalHashId(_, _, _, _, _, _, _, undefined, _, _) -> false;
evalHashId(CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, EvTimestamp) ->
    Transactions = hashIdTransactions(HashId, TrxsGroups),
    TimeLimit = EvTimestamp - TimeMillis,
    routeOperation(CatOperation, TakeActual, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx).

%Tiempo actual en milisegundos
get_timestamp() ->
    {Mega, Sec, Micro} = os:timestamp(),
    (Mega*1000000 + Sec)*1000 + round(Micro/1000).

%Extrae las transacciones asociadas a un HashId
hashIdTransactions(HashId, TrxsGroups) ->
    HasKey = proplists:is_defined(HashId, TrxsGroups),
    defineHashIdTransactions(HasKey, HashId, TrxsGroups).

%Retorna las transacciones asociadas al key
%1er argumento indica si el key existe en los grupos de Transacciones
%Si existe retorna el valor asociado al key
%Si no existe retorna array vacío
defineHashIdTransactions(true, HashId, TrxsGroups) ->
    proplists:get_value(HashId, TrxsGroups);
defineHashIdTransactions(false, _, _) ->
    [].

%Rutea operaciones
%2da variable en true considero la Transacción actual
%2da variable en false considero la Transacción actual
routeOperation(<<"COUNT">>, true, TimeLimit, Transactions, Field, Operator, Values, _) ->
    operation("COUNT", validator(Operator, Values, 1), TimeLimit, Transactions, Field, Operator, Values, 1);
routeOperation(<<"COUNT">>, false, TimeLimit, Transactions, Field, Operator, Values, _) ->
    operation("COUNT", false, TimeLimit, Transactions, Field, Operator, Values, 0);
routeOperation(<<"SUM">>, true, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx) ->
    TrxValue = proplists:get_value(Field, CurrentTrx),
    operation("SUM", validator(Operator, Values, TrxValue), TimeLimit, Transactions, Field, Operator, Values, TrxValue);
routeOperation(<<"SUM">>, false, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx) ->
    operation("SUM", false, TimeLimit, Transactions, Field, Operator, Values, 0);
routeOperation(<<"AVG">>, true, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx) ->
    TrxValue = proplists:get_value(Field, CurrentTrx),
    operation("AVG", validator(Operator, Values, TrxValue), TimeLimit, Transactions, Field, Operator, Values, {TrxValue,1,TrxValue});
routeOperation(<<"AVG">>, false, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx) ->
    operation("AVG", false, TimeLimit, Transactions, Field, Operator, Values, {0,0,0}).

%Operaciones sobre histórico de las transacciones
%2do argumento en true - cumple el condicional
%2do argumento en false y 4to array vacío - no cumple el condicional
%En el último bloque ejecuta el conteo iterativo
operation(_, true, _, _, _, _, _, _) -> 
    true;
operation(_, false, _, [], _, _, _, _) -> 
    false;
operation("COUNT", Success, TimeLimit, Transactions, Field, Operator, Values, Total) -> 
    {Trx} = hd(Transactions),
    TimeTrx = proplists:get_value(<<"t">>, Trx),
    {Continue, NewTotal} = count(TimeLimit, TimeTrx, Total),
    checkContinueOperation(Continue, "COUNT", Operator, Values, NewTotal, NewTotal, TimeLimit, Transactions, Field);
operation("SUM", Success, TimeLimit, Transactions, Field, Operator, Values, Total) -> 
    {Trx} = hd(Transactions),
    TimeTrx = proplists:get_value(<<"t">>, Trx),
    TrxValue = proplists:get_value(Field, Trx),
    {Continue, NewTotal} = sum(TimeLimit, TimeTrx, Total, TrxValue),
    checkContinueOperation(Continue, "SUM", Operator, Values, NewTotal, NewTotal, TimeLimit, Transactions, Field);
operation("AVG", Success, TimeLimit, Transactions, Field, Operator, Values, Total) -> 
    {Trx} = hd(Transactions),
    TimeTrx = proplists:get_value(<<"t">>, Trx),
    TrxValue = proplists:get_value(Field, Trx),
    {Continue, NewTotal} = avg(TimeLimit, TimeTrx, Total, TrxValue),
    LAvg = element(3,NewTotal),
    checkContinueOperation(Continue, "AVG", Operator, Values, NewTotal, LAvg, TimeLimit, Transactions, Field).

%Ruteo a la operación
%Un false representa que se superó el tiempo límite por lo que no hay coincidencia
checkContinueOperation(false, _, _, _, _, _, _, _, _) ->
    false;
checkContinueOperation(true, OpValue, Operator, Values, NewTotal, TotalCompare, TimeLimit, Transactions, Field) ->  
    operation(OpValue, validator(Operator, Values, TotalCompare), TimeLimit, tl(Transactions), Field, Operator, Values, NewTotal).

%Cuenta la transacción solo si se encuentra en el límite de tiempo
count(TimeLimit, TimeTrx, Total) -> case (TimeTrx >= TimeLimit) of 
    true -> {true, (Total + 1)};
    false -> {false, Total} end.

%Suma el campo de la transacción solo si se encuentra en el límite de tiempo
sum(_, _, Total, undefined) -> Total;
sum(TimeLimit, TimeTrx, Total, TrxValue) -> case (TimeTrx >= TimeLimit) of 
    true -> {true, (Total + TrxValue)};
    false -> {false, Total} end.

%{TotalSum,TotalCount} = Total,
%Calcula el average solo si se encuentra en el límite de tiempo
avg(_, _, Total, undefined) -> Total;
avg(TimeLimit, TimeTrx, Total, TrxValue) -> case (TimeTrx >= TimeLimit) of 
    true -> {TotalSum,TotalCount,TotalAvg} = Total,
        LSum = TotalSum+TrxValue,
        LCount = TotalCount+1,
        LAvg = LSum/LCount,
        {true, {LSum,LCount,LAvg}};
    false -> {false, Total} end.

%Comprueba el condicional respecto al total
validator(<<"=:=">>, Values, Total) ->
    Value = hd(Values),
    (Total =:= Value);
validator(<<"=/=">>, Values, Total) ->
    Value = hd(Values),
    (Total =/= Value);
validator(<<">">>, Values, Total) ->
    Value = hd(Values),
    (Total > Value);
validator(<<"<">>, Values, Total) ->
    Value = hd(Values),
    (Total < Value);
validator(<<">=">>, Values, Total) ->
    Value = hd(Values),
    (Total >= Value);
validator(<<"=<">>, Values, Total) ->
    Value = hd(Values),
    (Total =< Value);
validator(<<"between">>, Values, Total) ->
    Value = hd(Values),
    (Total >= Value) and (Total =< Value);
validator(<<"in">>, Values, Total) ->
    validateMultipleIn(false, Values, Total);
validator(<<"!in">>, Values, Total) ->
    validateMultipleOut(false, Values, Total).

%Comprueba el condicional respecto al total - Valida si se encuentra en el array
validateMultipleIn(true, _, _) -> 
    true;
validateMultipleIn(false, [], _) -> 
    false;
validateMultipleIn(Result, Values, Total) -> 
    Value = hd(Values),
    validateMultipleIn((Total =:= Value), tl(Values), Total).

%Comprueba el condicional respecto al total - Valida que no se encuentre en el array
validateMultipleOut(false, _, _) -> 
    false;
validateMultipleOut(true, [], _) -> 
    true;
validateMultipleOut(Result, Values, Total) -> 
    Value = hd(Values),
    validateMultipleOut((Total =/= Value), tl(Values), Total).

%%%......................................................................................................................................
%%%......................................................................................................................................
%%%......................................................................................................................................

%%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%%%Reglas categoría 2 de tipo GroupBy
%%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%Evalúa un criterio categoría 2 de tipo GroupBy
%Los argumentos son especificados en la invocación - Construida en Node
%Arg 1 - Tipo de Operación a ejecutar SUM, COUNT, AVG
%Arg 2 - Considerar o no la transacción actual
%Arg 3 - Campos que se utiliza para el agrupamiento
%Arg 4 - Campos que se en en la sumatoria o el average
%Arg 5 - Operador de consulta >, <, >=, etc
%Arg 6 - Valores comparativos
%Arg 7 - Límite de tiempo expresado en milisegundos
%Arg 8 - HashId asociado al criterio
%Arg 9 - Json de Grupos de Transacciones
%Arg 10 - Transacción actual
%Arg 11 - Listado de HashId que cumplieron
evalGroupBy(_, _, _, _, _, _, _, _, undefined, _, _, _) -> false;
evalGroupBy(CatOperation, TakeActual, GroupFields, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp) ->
    Separator = keySeparator(),
    routeExecEvalGroupBy(checkTakeActual(TakeActual, HashId, HashIdList), 
        CatOperation, TakeActual, GroupFields, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, HashIdList, Separator, EvTimestamp).

%Rutea a la evaluación de reglas categoría 2 de tipo GroupBy o retorna false inmediatamente
%El primer armento se determina al evaluar o no la transacción actual (key TakeActual)
routeExecEvalGroupBy(false, _, _, _, _, _, _, _, _, _, _, _, _, _) ->
    false;
routeExecEvalGroupBy(true, CatOperation, TakeActual, GroupFields, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, HashIdList, Separator, EvTimestamp) ->
    %execEvalCategory2GroupBy(CatOperation, TakeActual, GroupFields, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, HashIdList, Separator).
    Transactions = hashIdTransactionsGroupBy(HashId, TrxsGroups, GroupFields, CurrentTrx, Separator),
    TimeLimit = EvTimestamp - TimeMillis,
    routeOperation(CatOperation, TakeActual, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx).

%Extrae las transacciones asociadas a un HashId
hashIdTransactionsGroupBy(HashId, TrxsGroups, GroupFields, CurrentTrx, Separator) ->
    HasKey = proplists:is_defined(HashId, TrxsGroups),
    defineHashIdTransactionsGroupBy(HasKey, HashId, TrxsGroups, GroupFields, CurrentTrx, Separator).

%Retorna las transacciones asociadas al key
%1er argumento indica si el key existe en los grupos de Transacciones
%Si existe retorna el valor asociado al key y a los GroupFields
%Si no existe retorna array vacío
defineHashIdTransactionsGroupBy(true, HashId, TrxsGroups, GroupFields, CurrentTrx, Separator) ->
    {HashIdTrxs} = proplists:get_value(HashId, TrxsGroups),
    HasGroupBy = proplists:is_defined(<<"groupBy">>, HashIdTrxs),
    extractGroupByTransactions(HasGroupBy, HashIdTrxs, GroupFields, CurrentTrx, Separator);
defineHashIdTransactionsGroupBy(false,_, _,_,_,_) ->
    [].

%Extrae el grupo de transacciones del espacio asociadas al HashId que se procesa
%Listado de argumentos
%Arg 1 - Indicador de si el HashId en el espacio presenta el key "groupBy" (Si no retorna array vacío)
%Arg 2 - Objeto asociado al HashId en el espacio
%Arg 3 - Elementos de agrupación
%Arg 4 - Transacción que se evalúa
%Arg 5 - Elemento separador de los keys
extractGroupByTransactions(true, HashIdTrxs, GroupFields, CurrentTrx, Separator) ->
    {HashIdTrxsGroupBy} = proplists:get_value(<<"groupBy">>, HashIdTrxs),
    GField = groupByKeyBuilder(hd(GroupFields), tl(GroupFields), CurrentTrx, Separator, ""),
    transactionsByGroup(proplists:get_value(GField, HashIdTrxsGroupBy));
extractGroupByTransactions(false, _, _, _, _) ->
    [].

%Retorna el listado de transacciones asociados al HasId y al grupo que se procesa
%Un valor undefined indica que el campo aún no se encuentra en el espacio por lo que retorna array vacío
transactionsByGroup(undefined) ->
    [];
transactionsByGroup(Transactions) ->
    Transactions.

%Evalúa un criterio categoría 2 GroupBy para un elemento HASHID
%Los argumentos son especificados en la invocación - Construida en Node
%Arg 1 - Tipo de Operación a ejecutar SUM, COUNT, AVG
%Arg 2 - Considerar o no la transacción actual
%Arg 3 - Campos que se utiliza para el agrupamiento
%Arg 4 - Campo que se utiliza en la sumatoria o el average
%Arg 5 - Operador de consulta >, <, >=, etc
%Arg 6 - Valores comparativos
%Arg 7 - Límite de tiempo expresado en milisegundos
%Arg 8 - HashId asociado al criterio
%Arg 9 - Json de Grupos de Transacciones
%Arg 10 - Transacción actual
evalHashIdGroupBy(_, _, _, _, _, _, _, _, undefined, _, _) -> false;
evalHashIdGroupBy(CatOperation, TakeActual, GroupFields, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, EvTimestamp) ->
    Separator = keySeparator(),
    Transactions = hashIdTransactionsGroupBy(HashId, TrxsGroups, GroupFields, CurrentTrx, Separator),
    TimeLimit = EvTimestamp - TimeMillis,
    routeOperation(CatOperation, TakeActual, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx).

%%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%%%Fin Reglas categoría 2 de tipo GroupBy
%%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%Key separador de los elementos de agrupación
keySeparator() -> "_".

%Construye el key a través del cual se encuentran agrupadas las transacciones en el Espacio para un determinado HashId
%Arg 1 - Elemento actual del grupo a procesar
%Arg 2 - Elementos restantes del grupo a procesar
%Arg 3 - Transacción actual que se procesa
%Arg 4 - Elemento separador de los campos
%Arg 5 - Resultado del proceso - Key agrupado
groupByKeyBuilder(CurrentGroup, [], CurrentTrx, Separator, Result) -> 
    Field = proplists:get_value(CurrentGroup, CurrentTrx),
    list_to_binary(Result ++ binary_to_list(Field));
groupByKeyBuilder(CurrentGroup, Groups, CurrentTrx, Separator, Result) -> 
    Field = proplists:get_value(CurrentGroup, CurrentTrx),
    NResult = Result ++ binary_to_list(Field) ++ Separator,
    groupByKeyBuilder(hd(Groups), tl(Groups), CurrentTrx, Separator, NResult).

%%%......................................................................................................................................
%%%......................................................................................................................................
%%%......................................................................................................................................

%%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%%%Reglas categoría 2 de tipo LAST
%%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%Evalúa una condicional LAST sencilla (Que no forma parte de agrupadores)
%Arg 1 - Campo de la transacción con que se establece la comparación
%Arg 2 - Operador comparativo - Solo permitidos <, >, =, !=
%Arg 3 - Transacción actual que se evalúa
%Arg 4 - Última transacción evaluada
evalSingleLast(_, _, _, undefined) ->
    false;
evalSingleLast(Field, Operator, CurrentTrx, LastTrx) ->
    CurrentField = proplists:get_value(Field, CurrentTrx),
    LastField = proplists:get_value(Field, LastTrx),
    applyEvalSingleLast(CurrentField, LastField, Operator).

%Ejecuta la evaluación de la condicional LAST
%Arg 1 - Valor del campo de la transacción que se evalúa
%Arg 2 - Valor del campo de la última transacción evaluada
%Arg 3 - Operador comparativo
%Si el argumento 1 y/o el 2 son undefined se retorna false
applyEvalSingleLast(undefined, _, _) ->
    false;
applyEvalSingleLast(_, undefined, _) ->
    false;
applyEvalSingleLast(CurrentField, LastField, Operator) ->
    singleValidator(Operator, CurrentField, LastField).

%Comparador reducido a los operadores permitidos de los condicionales LAST
%Arg 1 - Operador comparativo
%Arg 2 - Valor comparativo
%Arg 3 - Valor base en la comparacion
singleValidator(<<"=:=">>, Left, Right) ->
    (Left =:= Right);
singleValidator(<<"=/=">>, Left, Right) ->
    (Left =/= Right);
singleValidator(<<">">>, Left, Right) ->
    (Left > Right);
singleValidator(<<"<">>, Left, Right) ->
    (Left < Right).

%Evaluación de un condicional de categoría 2 que cuente con operadores LAST
%Arg 1 - Tipo de Operación a ejecutar SUM, COUNT, AVG
%Arg 2 - Considerar o no la transacción actual
%Arg 3 - Campo que se utiliza en la sumatoria o el average
%Arg 4 - Operador de consulta >, <, >=, etc
%Arg 5 - Valores comparativos
%Arg 6 - Límite de tiempo expresado en milisegundos
%Arg 7 - HashId asociado al criterio
%Arg 8 - Json de Grupos de Transacciones
%Arg 9 - Transacción actual
%Arg 10 - Listado de HashId que cumplieron
%Arg 11 - Operadores LAST que se utilizan en la evaluación de la categoría
evalCategory2Last(_, _, _, _, _, _, _, undefined, _, _, _, _) -> false;
evalCategory2Last(CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, HashIdList, LastConditions, EvTimestamp) ->
    routeExecEvalCat2Last(checkTakeActual(TakeActual, HashId, HashIdList), 
        CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, LastConditions, EvTimestamp).

%Rutea a la evaluación de reglas categoría 2 con operadores LAST o retorna false inmediatamente
%El primer armento se determina al evaluar o no la transacción actual (key TakeActual)
%Los restantes argumentos son los mismos de la función "evalCategory2Last"
routeExecEvalCat2Last(false, _, _, _, _, _, _, _, _, _, _, _) ->
    false;
routeExecEvalCat2Last(true, CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, LastConditions, EvTimestamp) ->
    %execEvalCat2Last(CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, LastConditions).
    Transactions = hashIdTransactions(HashId, TrxsGroups),
    %TimeLimit = get_timestamp() - TimeMillis,
    TimeLimit = EvTimestamp - TimeMillis,
    routeOperationLast(CatOperation, TakeActual, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions).

%Evalua un elemento de categoria 2
%Obtiene el listado de transacciones vinculado al HashId, estos se utilizan en el cálculos de los operadores de agrupación
%Los mismos argumentos de la función "evalCategory2Last"
%execEvalCat2Last(CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, LastConditions) ->
%   Transactions = hashIdTransactions(HashId, TrxsGroups),
%   TimeLimit = get_timestamp() - TimeMillis,
%   routeOperationLast(CatOperation, TakeActual, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions).

%Evaluación de un condicional de categoría 2 de tipo GroupBy que cuente con operadores LAST
%Se utiliza un caracter separador para la definición del elemento GroupBy
%La función "checkTakeActual" determina si se debe considerar la transacción actual en los cálculos
%Si se debe considerar se determina si la transacción actual cumplió con los condicionales del agrupador
%Arg 1 - Tipo de Operación a ejecutar SUM, COUNT, AVG
%Arg 2 - Considerar o no la transacción actual
%Arg 3 - Campos que se utiliza para el agrupamiento
%Arg 4 - Campos que se en en la sumatoria o el average
%Arg 5 - Operador de consulta >, <, >=, etc
%Arg 6 - Valores comparativos
%Arg 7 - Límite de tiempo expresado en milisegundos
%Arg 8 - HashId asociado al criterio
%Arg 9 - Json de Grupos de Transacciones
%Arg 10 - Transacción actual
%Arg 11 - Listado de HashId que cumplieron
%Arg 12 - Operadores LAST que se utilizan en la evaluación de la categoría
evalGroupByLast(_, _, _, _, _, _, _, _, undefined, _, _, _, _) -> false;
evalGroupByLast(CatOperation, TakeActual, GroupFields, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, HashIdList, LastConditions, EvTimestamp) ->
    Separator = keySeparator(),
    routeExecEvalGroupByLast(checkTakeActual(TakeActual, HashId, HashIdList), 
        CatOperation, TakeActual, GroupFields, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, Separator, LastConditions, EvTimestamp).

%Rutea a la evaluación de reglas categoría 2 de tipo GroupBy o retorna false inmediatamente
%El primer armento se determina al evaluar o no la transacción actual (key TakeActual)
%Los restantes argumentos son los mismos de la función "evalGroupByLast"
routeExecEvalGroupByLast(false, _, _, _, _, _, _, _, _, _, _, _, _, _) ->
    false;
routeExecEvalGroupByLast(true, CatOperation, TakeActual, GroupFields, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, Separator, LastConditions, EvTimestamp) ->
    Transactions = hashIdTransactionsGroupBy(HashId, TrxsGroups, GroupFields, CurrentTrx, Separator),
    TimeLimit = EvTimestamp - TimeMillis,
    routeOperationLast(CatOperation, TakeActual, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions).

%Evalúa un criterio categoría 2 para un elemento HASHID que contiene condiciones LAST
%Los argumentos son especificados en la invocación - Construida en Node
%Arg 1 - Tipo de Operación a ejecutar SUM, COUNT, AVG
%Arg 2 - Considerar o no la transacción actual
%Arg 3 - Campo que se utiliza en la sumatoria o el average
%Arg 4 - Operador de consulta >, <, >=, etc
%Arg 5 - Valores comparativos
%Arg 6 - Límite de tiempo expresado en milisegundos
%Arg 7 - HashId asociado al criterio
%Arg 8 - Json de Grupos de Transacciones
%Arg 9 - Transacción actual
%Arg 10 - Operadores LAST que se utilizan en la evaluación de la categoría
evalHashIdLast(_, _, _, _, _, _, _, undefined, _, _, _) -> false;
evalHashIdLast(CatOperation, TakeActual, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, LastConditions, EvTimestamp) ->
    Transactions = hashIdTransactions(HashId, TrxsGroups),
    TimeLimit = EvTimestamp - TimeMillis,
    routeOperationLast(CatOperation, TakeActual, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions).

%Evalúa un criterio categoría 2 de tipo GroupBy para un elemento HASHID que contiene condiciones LAST
%Los argumentos son especificados en la invocación - Construida en Node
%Arg 1 - Tipo de Operación a ejecutar SUM, COUNT, AVG
%Arg 2 - Considerar o no la transacción actual
%Arg 3 - Campos que se utilizan para el agrupamiento
%Arg 4 - Campo que se utiliza en la sumatoria o el average
%Arg 5 - Operador de consulta >, <, >=, etc
%Arg 6 - Valores comparativos
%Arg 7 - Límite de tiempo expresado en milisegundos
%Arg 8 - HashId asociado al criterio
%Arg 9 - Json de Grupos de Transacciones
%Arg 10 - Transacción actual
%Arg 11 - Operadores LAST que se utilizan en la evaluación de la categoría
evalHashIdGroupByLast(_, _, _, _, _, _, _, _, undefined, _, _, _) -> false;
evalHashIdGroupByLast(CatOperation, TakeActual, GroupFields, Field, Operator, Values, TimeMillis, HashId, TrxsGroups, CurrentTrx, LastConditions, EvTimestamp) ->
    Separator = keySeparator(),
    Transactions = hashIdTransactionsGroupBy(HashId, TrxsGroups, GroupFields, CurrentTrx, Separator),
    TimeLimit = EvTimestamp - TimeMillis,
    routeOperationLast(CatOperation, TakeActual, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions).

%Rutea operaciones para tipo LAST
%Arg 1 - Operador acumulador SUM, COUNT, AVG
%Arg 2 - Valor del TakeActual | true considero la Transacción actual | false no considero la Transacción actual
%Arg 3 - Fecha límite (en milisegundos) de las transacciones en el espacio que se deben considerar
%Arg 4 - Transacciones del espacio que se deben considerar en el operador acumulador
%Arg 5 - Campo que se debe considerar en la operación acumulador | Solo aplica para las operaciones de SUM y AVG
%Arg 6 - Operador comparativo
%Arg 7 - Valor(es) comparativo(s), este es el que se compara respecto al acumulado
%Arg 8 - Transacción actual
%Arg 9 - Operadores LAST que se utilizan en la evaluación de la categoría
%Casos del TakeActual
%Para el COUNT con TakeActual = true el conteo inicia en 1 y la última transacción corresponde a la transacción que se evalúa 
%Para el COUNT con TakeActual = false el conteo inicia en 0 y la última transacción corresponde a la primer transacción del espacio
%Para el SUM con TakeActual = true la sumatoria inicia en con el valor del campo especificado en "Field" de la transacción actual 
%%      y la última transacción corresponde a la transacción que se evalúa 
%Para el SUM con TakeActual = false la sumatoria inicia en 0 y la última transacción corresponde a la primer transacción del espacio
%Para el AVG con TakeActual = true el promedio inicia en con el valor del campo especificado en "Field" de la transacción actual 
%%      y la última transacción corresponde a la transacción que se evalúa 
%Para el AVG con TakeActual = false el promedio inicia en 0 y la última transacción corresponde a la primer transacción del espacio
routeOperationLast(<<"COUNT">>, true, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions) ->
    operationLast("COUNT", validator(Operator, Values, 1), TimeLimit, residualTrxsLast(Transactions), firstElementList(Transactions), 
        CurrentTrx, false, Field, Operator, Values, 1, LastConditions);
routeOperationLast(<<"COUNT">>, false, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions) ->
    BaseTransaction = firstElementList(Transactions),
    operationLast("COUNT", false, TimeLimit, residualTrxsLast(Transactions), BaseTransaction, 
        BaseTransaction, true, Field, Operator, Values, 0, LastConditions);
routeOperationLast(<<"SUM">>, true, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions) ->
    TrxValue = proplists:get_value(Field, CurrentTrx),
    operationLast("SUM", validator(Operator, Values, TrxValue), TimeLimit, residualTrxsLast(Transactions), firstElementList(Transactions), 
        CurrentTrx, false, Field, Operator, Values, TrxValue, LastConditions);
routeOperationLast(<<"SUM">>, false, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions) ->
    BaseTransaction = firstElementList(Transactions),
    operationLast("SUM", false, TimeLimit, residualTrxsLast(Transactions), BaseTransaction, 
        BaseTransaction, true, Field, Operator, Values, 0, LastConditions);
routeOperationLast(<<"AVG">>, true, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions) ->
    TrxValue = proplists:get_value(Field, CurrentTrx),
    operationLast("AVG", validator(Operator, Values, TrxValue), TimeLimit, residualTrxsLast(Transactions), firstElementList(Transactions), 
        CurrentTrx, false, Field, Operator, Values, {TrxValue,1,TrxValue}, LastConditions);
routeOperationLast(<<"AVG">>, false, TimeLimit, Transactions, Field, Operator, Values, CurrentTrx, LastConditions) ->
    BaseTransaction = firstElementList(Transactions),
    operationLast("AVG", false, TimeLimit, residualTrxsLast(Transactions), BaseTransaction, 
        BaseTransaction, true, Field, Operator, Values, {0,0,0}, LastConditions).

%Comprueba el condicional y rutea el acumulador en caso de ser necesario | Cada ruteo comprueba la fecha de limite respecto al espacio
%Arg 1 - Operador acumulador SUM, COUNT, AVG
%Arg 2 - Flag de si se cumplió o no el condicional | true - cumple el condicional |  false, Arg 4 array vacío y Arg 5 undefined - no cumple el condicional
%Arg 3 - Fecha límite (en milisegundos) de las transacciones en el espacio que se deben considerar
%Arg 4 - Transacciones del espacio que se deben considerar en el operador acumulador
%Arg 5 - Transacción que se procesa actualmente
%Arg 6 - Transacción anterior - Referencia
%Arg 7 - Determina si es el primer elemento que se procesa | true = no realiza comparación respecto a una transacción previa
%Arg 8 - Campo que se debe considerar en la operación acumulador | Solo aplica para las operaciones de SUM y AVG
%Arg 9 - Operador comparativo
%Arg 10 - Valor(es) comparativo(s), este es el que se compara respecto al acumulado
%Arg 11 - Total acumulado
%Arg 12 - Operadores LAST que se utilizan en la evaluación de la categoría
%En el último bloque ejecuta el conteo iterativo
operationLast(_, true, _, _, _, _, _, _, _, _, _, _) -> 
    true;
operationLast(_, false, _, [], undefined, _, _, _, _, _, _, _) -> 
    false;
operationLast("COUNT", false, TimeLimit, Transactions, BaseTransaction, PreviousTrx, First, Field, Operator, Values, Total, LastConditions) -> 
    TimeTrx = proplists:get_value(<<"t">>, BaseTransaction),
    checkLimitAndRouteLast((TimeTrx >= TimeLimit), "COUNT", First, TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, 
        Values, Total, LastConditions);
operationLast("SUM", false, TimeLimit, Transactions, BaseTransaction, PreviousTrx, First, Field, Operator, Values, Total, LastConditions) -> 
    TimeTrx = proplists:get_value(<<"t">>, BaseTransaction),
    checkLimitAndRouteLast((TimeTrx >= TimeLimit), "SUM", First, TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, 
        Values, Total, LastConditions);
operationLast("AVG", false, TimeLimit, Transactions, BaseTransaction, PreviousTrx, First, Field, Operator, Values, Total, LastConditions) ->
    TimeTrx = proplists:get_value(<<"t">>, BaseTransaction),
    checkLimitAndRouteLast((TimeTrx >= TimeLimit), "AVG", First, TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, 
        Values, Total, LastConditions).

%Comprueba el límite permitido del espacio y rutea el acumulador
%Arg 1 - Flag que indica si se superó el límite permitido | true = continúa el acumulado | false = detiene el acumulado y responde falso al condicional
%Arg 2 - Operador acumulador SUM, COUNT, AVG
%Arg 3 - Determina si es el primer elemento que se procesa | true = no realiza comparación respecto a una transacción previa
%Arg 4 - Fecha límite (en milisegundos) de las transacciones en el espacio que se deben considerar
%Arg 5 - Transacciones del espacio que se deben considerar en el operador acumulador
%Arg 6 - Transacción que se procesa actualmente
%Arg 7 - Transacción anterior - Referencia
%Arg 8 - Campo que se debe considerar en la operación acumulador | Solo aplica para las operaciones de SUM y AVG
%Arg 9 - Operador comparativo
%Arg 10 - Valor(es) comparativo(s), este es el que se compara respecto al acumulado
%Arg 11 - Total acumulado
%Arg 12 - Operadores LAST que se utilizan en la evaluación de la categoría
checkLimitAndRouteLast(false, _, _, _, _, _, _, _, _, _, _, _) ->
    false;
checkLimitAndRouteLast(true, "COUNT", First, TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, Values, Total, LastConditions) ->
    checkFirstTrxAndRoute(First, "COUNT", TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, Values, Total, LastConditions);
checkLimitAndRouteLast(true, "SUM", First, TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, Values, Total, LastConditions) ->
    checkFirstTrxAndRoute(First, "SUM", TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, Values, Total, LastConditions);
checkLimitAndRouteLast(true, "AVG", First, TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, Values, Total, LastConditions) ->
    checkFirstTrxAndRoute(First, "AVG", TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, Values, Total, LastConditions).

%Ejecuta el acumulado respectivo | Valida el cumplimiento del condicional
%Arg 1 - Flag que indica si es el primer elemento que se procesa | true = no realiza comparación respecto a una transacción previa
%Arg 2 - Operador acumulador SUM, COUNT, AVG
%Arg 3 - Fecha límite (en milisegundos) de las transacciones en el espacio que se deben considerar
%Arg 4 - Transacciones del espacio que se deben considerar en el operador acumulador
%Arg 5 - Transacción que se procesa actualmente
%Arg 6 - Transacción anterior - Referencia
%Arg 7 - Campo que se debe considerar en la operación acumulador | Solo aplica para las operaciones de SUM y AVG
%Arg 8 - Operador comparativo
%Arg 9 - Valor(es) comparativo(s), este es el que se compara respecto al acumulado
%Arg 10 - Total acumulado
%Arg 11 - Operadores LAST que se utilizan en la evaluación de la categoría
%Función "validateLastConditions" valida la transacción actual vs la transacción previa respecto a los criterios definidos en "LastConditions"
%Acumulador COUNT - First = false | Valida si se debe considerar la transacción actual del espacio (cumple los condionales LAST)
%       e incrementa en uno el total si se cumplen dichos criterios
%Acumulador SUM - First = false | Valida si se debe considerar la transacción actual del espacio (cumple los condionales LAST)
%       y si se cumplen dichos criterios suma al total el campo de la transacción que se procesa
%Acumulador AVG - First = false | Valida si se debe considerar la transacción actual del espacio (cumple los condionales LAST)
%       y si se cumplen dichos criterios considera el valor del campo de la transacción que se procesa al promedio acumulado
checkFirstTrxAndRoute(true, "COUNT", TimeLimit, Transactions, BaseTransaction, _, Field, Operator, Values, Total, LastConditions) ->
    operationLast("COUNT", validator(Operator, Values, 1), TimeLimit, residualTrxsLast(Transactions), firstElementList(Transactions), 
        BaseTransaction, false, Field, Operator, Values, 1, LastConditions);
checkFirstTrxAndRoute(false, "COUNT", TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, Values, Total, LastConditions) ->
    Assert = validateLastConditions(true, firstElementList(LastConditions), tl(LastConditions), BaseTransaction, PreviousTrx),
    NTotal = countLast(Assert, Total),
    operationLast("COUNT", validator(Operator, Values, NTotal), TimeLimit, residualTrxsLast(Transactions), firstElementList(Transactions), 
        definePreviousTrx(Assert, PreviousTrx, BaseTransaction), false, Field, Operator, Values, NTotal, LastConditions);
checkFirstTrxAndRoute(true, "SUM", TimeLimit, Transactions, BaseTransaction, _, Field, Operator, Values, Total, LastConditions) ->
    TrxValue = proplists:get_value(Field, BaseTransaction),
    operationLast("SUM", validator(Operator, Values, TrxValue), TimeLimit, residualTrxsLast(Transactions), firstElementList(Transactions), 
        BaseTransaction, false, Field, Operator, Values, TrxValue, LastConditions);
checkFirstTrxAndRoute(false, "SUM", TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, Values, Total, LastConditions) ->
    TrxValue = proplists:get_value(Field, BaseTransaction),
    Assert = validateLastConditions(true, firstElementList(LastConditions), tl(LastConditions), BaseTransaction, PreviousTrx),
    NTotal = sumLast(Assert, Total, TrxValue),
    operationLast("SUM", validator(Operator, Values, NTotal), TimeLimit, residualTrxsLast(Transactions), firstElementList(Transactions), 
        definePreviousTrx(Assert, PreviousTrx, BaseTransaction), false, Field, Operator, Values, NTotal, LastConditions);
checkFirstTrxAndRoute(true, "AVG", TimeLimit, Transactions, BaseTransaction, _, Field, Operator, Values, Total, LastConditions) ->
    TrxValue = proplists:get_value(Field, BaseTransaction),
    operationLast("AVG", validator(Operator, Values, TrxValue), TimeLimit, residualTrxsLast(Transactions), firstElementList(Transactions), 
        BaseTransaction, false, Field, Operator, Values, {TrxValue,1,TrxValue}, LastConditions);
checkFirstTrxAndRoute(false, "AVG", TimeLimit, Transactions, BaseTransaction, PreviousTrx, Field, Operator, Values, Total, LastConditions) ->
    TrxValue = proplists:get_value(Field, BaseTransaction),
    Assert = validateLastConditions(true, firstElementList(LastConditions), tl(LastConditions), BaseTransaction, PreviousTrx),
    NTotal = avgLast(Assert, Total, TrxValue),
    LAvg = element(3,NTotal),
    operationLast("AVG", validator(Operator, Values, LAvg), TimeLimit, residualTrxsLast(Transactions), firstElementList(Transactions), 
        definePreviousTrx(Assert, PreviousTrx, BaseTransaction), false, Field, Operator, Values, NTotal, LastConditions).

%Extrae el primer elemento de una lista, en caso de lista vacía retorna undefined
firstElementList([]) -> undefined;
firstElementList(List) -> 
    {Element} = hd(List),
    Element.

%Retorna los elementos de la lista a excepción del primero, en caso de lista vacía se retorna a si misma
residualTrxsLast([]) -> [];
residualTrxsLast(Transactions) -> tl(Transactions).

%Define la transacción previa que se utiliza en las operaciones LAST
%Arg 1 - Indica si la transacción actual debe convertirse en la transacción previa. En caso de false se retorna la transacción previa y no la actual
%Arg 2 - Transacción previa
%Arg 3 - Transacción actual
definePreviousTrx(false, PreviousTrx, _) -> PreviousTrx;
definePreviousTrx(true, _, BaseTransaction) -> BaseTransaction.

%Valida los condicionales LAST respecto a la transacción actual y a la transacción anterior
%Arg 1 - Indica si el anterior criterio se cumplió | false - detiene el flujo de validación y responde false
%Arg 2 - Criterio actual que se evalúa
%Arg 3 - Criterios restantes a evaluar | Array vacío indica que se procesa el último criterio - responde el resultado de la evaluación
%Arg 4 - Transacción actual que se procesa
%Arg 5 - Transacción anterior
validateLastConditions(false, _, _, _, _) ->
    false;
validateLastConditions(LastResult, Current, [], CurrentTrx, LastTrx) ->
    Field = proplists:get_value(<<"field">>, Current),
    Operator = proplists:get_value(<<"operator">>, Current),
    CurrentField = proplists:get_value(Field, CurrentTrx),
    LastField = proplists:get_value(Field, LastTrx),
    applyEvalSingleLast(CurrentField, LastField, Operator);
validateLastConditions(LastResult, Current, Others, CurrentTrx, LastTrx) ->
    Field = proplists:get_value(<<"field">>, Current),
    Operator = proplists:get_value(<<"operator">>, Current),
    CurrentField = proplists:get_value(Field, CurrentTrx),
    LastField = proplists:get_value(Field, LastTrx),
    Result = applyEvalSingleLast(CurrentField, LastField, Operator),
    validateLastConditions(Result, hd(Others), tl(Others), CurrentTrx, LastTrx).

%Cuenta la transacción solo si cumplió los condicionales LAST
%Arg 1 - Indicador de si debe o no contar. Este se determina validando los condicionales LAST la transacción actual vs la predecesora
%Arg 2 - Acumulado del conteo
countLast(false, Total) -> Total;
countLast(true, Total) -> Total + 1.

%Sumatoria de campo en la transacción solo si cumplió los condicionales LAST
%Arg 1 - Indicador de si debe o no sumar. Este se determina validando los condicionales LAST la transacción actual vs la predecesora
%Arg 2 - Acumulado de la sumatoria
%Arg 3 - Valor a sumar
sumLast(false, Total, _) -> Total;
sumLast(true, Total, TrxValue) -> Total + TrxValue.

%Average para un campo en la transacción solo si cumplió los condicionales LAST
%Arg 1 - Indicador de si debe o no sumar. Este se determina validando los condicionales LAST la transacción actual vs la predecesora
%Arg 2 - Acumulado del porcentaje. Se compone de Total sumado|Total de elementos|Average actual
%Arg 3 - Valor a considerar en el average
avgLast(false, Total, _) -> Total;
avgLast(true, Total, TrxValue) ->
    {TotalSum,TotalCount,TotalAvg} = Total,
    LSum = TotalSum+TrxValue,
    LCount = TotalCount+1,
    LAvg = LSum/LCount,
    {LSum,LCount,LAvg}.

%%%......................................................................................................................................
%%%......................................................................................................................................
%%%......................................................................................................................................

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(Start, TotalBloque, Total) ->
    mnesia:wait_for_tables([customer], 3),
    evaluator_sup:start_link(),
    install(),
    fillSpawned(Start, TotalBloque, Total).

start(_StartType, _StartArgs) ->
    mnesia:wait_for_tables([customer], 3),
    evaluator_sup:start_link().

stop(_State) ->
    ok.

initializeNameSpace(Nodes) ->
    mnesia:stop(),
    CreateSchemaResult = case mnesia:create_schema(Nodes) of
                            {error,{_, {already_exists,_}}} -> already_exists;
                            ok -> ok;
                            _ -> {unknown_result}
                        end,
    mnesia:start(),

                             %mnesia:create_table(customer, [{attributes, record_info(fields, customer)}, {index, [#customer.id]}, {disc_copies, Nodes}, {storage_properties, [{ets, [compressed]}, {dets, [{auto_save, 5000}]} ]}]),
                             %mnesia:create_table(customer, [{disc_copies, [node()]}, {record_name, connection},{storage_properties, [{ets, [compressed]}, {dets, [{auto_save, 5000}]} ]}]).
                             %mnesia:create_table(customer, [{attributes, record_info(fields, customer)}, {disc_copies, Nodes}, {storage_properties, [{ets, [compressed]}, {dets, [{auto_save, 5000}]}]}]),
    %CreateTableResult = case mnesia:create_table(customer, [{ram_copies, [node()]}, {disc_only_copies, nodes()}, {attributes, record_info(fields, customer)}, {storage_properties, [{ets, [compressed]}, {dets, [{auto_save, 5000}]} ]}])
    CreateTableResult = case mnesia:create_table(customer, [{frag_properties, [{node_pool, [node()]}, {n_fragments, 40}, {n_disc_only_copies, 1}]}, {attributes, record_info(fields, customer)}, {storage_properties, [{ets, [compressed]}, {dets, [{auto_save, 5000}]}]}])
                        of
                            {atomic,ok} -> {atomic,ok};
                            {aborted, {already_exists,_}} -> {already_exists, customer};
                            X -> X
                        end,
    [{create_schema_result, CreateSchemaResult}, {create_table_result, CreateTableResult}].

install() ->
    initializeNameSpace([node()]).

%%%-------------------------------------------------------------------
%%%------------------SEGMENTO GENERADO DINAMICAMENTE------------------
%%%-------------------------------------------------------------------

rule_hashId_0cdc25b536f0c0f52e54f90402c11e17(TrxsGroups, CurrentTrx, EvTimestamp) -> case ( (valueJson(<<"Pais">>, CurrentTrx) =:= <<"USA">>) and (valueJson(<<"Monto">>, CurrentTrx) > 1000) and (valueJson(<<"Moneda">>, CurrentTrx) =:= <<"$">>)) of  true -> {true,<<"0cdc25b536f0c0f52e54f90402c11e17">>,""}; false -> {false,<<"0cdc25b536f0c0f52e54f90402c11e17">>,""} end.
rule_1(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of  true -> Pid ! {1, true, "1"}; false -> Pid ! {2, false, "1"} end.
rule_hashId_0cdc25b536f0c0f52e54f90402c11e18(TrxsGroups, CurrentTrx, EvTimestamp) -> case ( (valueJson(<<"Pais">>, CurrentTrx) =:= <<"USA">>) and (valueJson(<<"Monto">>, CurrentTrx) > 1000) and (valueJson(<<"Moneda">>, CurrentTrx) =:= <<"$">>)) of  true -> {true,<<"0cdc25b536f0c0f52e54f90402c11e18">>,""}; false -> {false,<<"0cdc25b536f0c0f52e54f90402c11e18">>,""} end.
rule_2(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of  true -> Pid ! {2, true, "2"}; false -> Pid ! {2, false, "2"} end.
rule_hashId_0cdc25b536f0c0f52e54f90402c11e19(TrxsGroups, CurrentTrx, EvTimestamp) -> case ( (valueJson(<<"Pais">>, CurrentTrx) =:= <<"USA">>) and (valueJson(<<"Monto">>, CurrentTrx) > 1000) and (valueJson(<<"Moneda">>, CurrentTrx) =:= <<"$">>)) of  true -> {true,<<"0cdc25b536f0c0f52e54f90402c11e19">>,groupByKeyBuilder(<<"Comercio">>, [], CurrentTrx, "_", "")}; false -> {false,<<"0cdc25b536f0c0f52e54f90402c11e19">>,""} end.
rule_3(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of  true -> Pid ! {3, true, "3"}; false -> Pid ! {2, false, "3"} end.
rule_hashId_0cdc25b536f0c0f52e54f90402c11e20(TrxsGroups, CurrentTrx, EvTimestamp) -> case ( (valueJson(<<"Pais">>, CurrentTrx) =:= <<"USA">>) and (valueJson(<<"Monto">>, CurrentTrx) > 1000) and (valueJson(<<"Moneda">>, CurrentTrx) =:= <<"$">>)) of  true -> {true,<<"0cdc25b536f0c0f52e54f90402c11e20">>,groupByKeyBuilder(<<"Comercio">>, [], CurrentTrx, "_", "")}; false -> {false,<<"0cdc25b536f0c0f52e54f90402c11e20">>,""} end.
rule_4(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of  true -> Pid ! {4, true, "4"}; false -> Pid ! {2, false, "4"} end.
rule_5(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of  true -> Pid ! {5, true, "5"}; false -> Pid ! {2, false, "5"} end.
rule_6(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of  true -> Pid ! {6, true, "6"}; false -> Pid ! {2, false, "6"} end.
rule_7(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of  true -> Pid ! {7, true, "7"}; false -> Pid ! {2, false, "7"} end.
rule_8(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of  true -> Pid ! {8, true, "8"}; false -> Pid ! {2, false, "8"} end.
rule_9(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of  true -> Pid ! {9, true, "9"}; false -> Pid ! {2, false, "9"} end.
rule_10(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of     true -> Pid ! {10, true, "10"}; false -> Pid ! {2, false, "10"} end.
rule_11(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                 true -> Pid ! {11, true, "11"}; false -> Pid ! {2, false, "11"} end.
rule_12(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                  true -> Pid ! {12, true, "12"}; false -> Pid ! {2, false, "12"} end.
rule_13(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of   true -> Pid ! {13, true, "13"}; false -> Pid ! {2, false, "13"} end.
rule_14(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of     true -> Pid ! {14, true, "14"}; false -> Pid ! {2, false, "14"} end.
rule_15(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of    true -> Pid ! {15, true, "15"}; false -> Pid ! {2, false, "15"} end.
rule_16(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of    true -> Pid ! {16, true, "16"}; false -> Pid ! {2, false, "16"} end.
rule_17(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                 true -> Pid ! {17, true, "17"}; false -> Pid ! {2, false, "17"} end.
rule_18(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                  true -> Pid ! {18, true, "18"}; false -> Pid ! {2, false, "18"} end.
rule_19(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of   true -> Pid ! {19, true, "19"}; false -> Pid ! {2, false, "19"} end.
rule_20(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of     true -> Pid ! {20, true, "20"}; false -> Pid ! {2, false, "20"} end.
rule_21(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                 true -> Pid ! {21, true, "21"}; false -> Pid ! {2, false, "21"} end.
rule_22(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                  true -> Pid ! {22, true, "22"}; false -> Pid ! {2, false, "22"} end.
rule_23(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of   true -> Pid ! {23, true, "23"}; false -> Pid ! {2, false, "23"} end.
rule_24(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of     true -> Pid ! {24, true, "24"}; false -> Pid ! {2, false, "24"} end.
rule_25(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of    true -> Pid ! {25, true, "25"}; false -> Pid ! {2, false, "25"} end.
rule_26(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of    true -> Pid ! {26, true, "26"}; false -> Pid ! {2, false, "26"} end.
rule_27(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                 true -> Pid ! {27, true, "27"}; false -> Pid ! {2, false, "27"} end.
rule_28(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                  true -> Pid ! {28, true, "28"}; false -> Pid ! {2, false, "28"} end.
rule_29(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of   true -> Pid ! {29, true, "29"}; false -> Pid ! {2, false, "29"} end.
rule_30(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of     true -> Pid ! {30, true, "30"}; false -> Pid ! {2, false, "30"} end.
rule_31(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                 true -> Pid ! {31, true, "31"}; false -> Pid ! {2, false, "31"} end.
rule_32(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                  true -> Pid ! {32, true, "32"}; false -> Pid ! {2, false, "32"} end.
rule_33(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of   true -> Pid ! {33, true, "33"}; false -> Pid ! {2, false, "33"} end.
rule_34(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of     true -> Pid ! {34, true, "34"}; false -> Pid ! {2, false, "34"} end.
rule_35(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of    true -> Pid ! {35, true, "35"}; false -> Pid ! {2, false, "35"} end.
rule_36(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of    true -> Pid ! {36, true, "36"}; false -> Pid ! {2, false, "36"} end.
rule_37(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                 true -> Pid ! {37, true, "37"}; false -> Pid ! {2, false, "37"} end.
rule_38(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                  true -> Pid ! {38, true, "38"}; false -> Pid ! {2, false, "38"} end.
rule_39(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of   true -> Pid ! {39, true, "39"}; false -> Pid ! {2, false, "39"} end.
rule_40(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of     true -> Pid ! {40, true, "40"}; false -> Pid ! {2, false, "40"} end.
rule_41(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                 true -> Pid ! {41, true, "41"}; false -> Pid ! {2, false, "41"} end.
rule_42(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                  true -> Pid ! {42, true, "42"}; false -> Pid ! {2, false, "42"} end.
rule_43(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of   true -> Pid ! {43, true, "43"}; false -> Pid ! {2, false, "43"} end.
rule_44(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of     true -> Pid ! {44, true, "44"}; false -> Pid ! {2, false, "44"} end.
rule_45(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of    true -> Pid ! {45, true, "45"}; false -> Pid ! {2, false, "45"} end.
rule_46(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of    true -> Pid ! {46, true, "46"}; false -> Pid ! {2, false, "46"} end.
rule_47(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, false, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e17">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                 true -> Pid ! {47, true, "47"}; false -> Pid ! {2, false, "47"} end.
rule_48(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalCategory2(<<"COUNT">>, true, <<"undefined">>, <<">=">>, [5], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e18">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of                  true -> Pid ! {48, true, "48"}; false -> Pid ! {2, false, "48"} end.
rule_49(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"SUM">>, false, [<<"Comercio">>], <<"Monto">>, <<">=">>, [11000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e19">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of   true -> Pid ! {49, true, "49"}; false -> Pid ! {2, false, "49"} end.
rule_50(TrxsGroups, CurrentTrx, HashIdList, LastTrx, EvTimestamp, Pid) -> case ( (evalGroupBy(<<"AVG">>, true, [<<"Comercio">>], <<"Monto">>, <<">=">>, [5000], 3600000, <<"0cdc25b536f0c0f52e54f90402c11e20">>, TrxsGroups, CurrentTrx, HashIdList, EvTimestamp)) and ((valueJson(<<"Bin">>, CurrentTrx) =:= <<"6010">>) or (valueJson(<<"Bin">>, CurrentTrx) =:= <<"6011">>))) of     true -> Pid ! {50, true, "50"}; false -> Pid ! {2, false, "50"} end.